module.exports = {
    count: 10,
    status: {
        empty: 'e',
        battleship: 'b',
        cruiser1: 'c1',
        cruiser2: 'c2',
        destroyer1: 'd1',
        destroyer2: 'd2',
        destroyer3: 'd3',
        submarine1: 's1',
        submarine2: 's2',
        submarine3: 's3',
        submarine4: 's4',
        // reserved: r,
    }
}