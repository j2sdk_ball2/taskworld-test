const prefix = '/api/v1'

module.exports = {
  nodeEnv: process.env.NODE_ENV || 'development',
  port: process.env.API_PORT || 5555,
  routes: {
    index: `${prefix}/`,
    start: `${prefix}/start/`,
    placeBattleship: `${prefix}/place/battleship/`,
  },
}