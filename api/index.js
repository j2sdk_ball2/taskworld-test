require('dotenv').config({
    path: 'env/.local',
    silent: true
})
const server = require('./server')
server.start()