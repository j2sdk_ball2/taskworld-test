const Log = require('../services/log')
const { status, count } = require('../../config/block')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

module.exports = {
    validate
}

async function validate(ctx, next) {
    const body = ctx.request.body
    logInfo(validate.name, `params: ${JSON.stringify(body)}`)
    console.log(validate.name, `params: ${JSON.stringify(body)}`)

    if (!Array.isArray(body)) {
        return null
    }

    let prevId
    for (const id of body) {

        if (prevId) {
            const diff = id - prevId

            if (diff != 1 && diff != count) {
                logInfo(validate.name, `validation failed, invalid format, id is not consecutive`)
                console.log(validate.name, `validation failed, invalid format, id is not consecutive`)

                ctx.status = 400
                return ctx.body = []
            }
        }

        prevId = id
    }

    logInfo(validate.name, `validation passed`)
    console.log(validate.name, `validation passed`)
    return next()
}