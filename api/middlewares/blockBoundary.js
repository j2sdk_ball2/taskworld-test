const Log = require('../services/log')
const { status, count } = require('../../config/block')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

module.exports = {
    validate,
    validateBlocksChange,
    validateBlocksStatus,
}

async function validate(ctx, next) {
    const body = ctx.request.body
    const length = ctx.blocksLength

    logInfo(validate.name, `params: ${JSON.stringify(body)}`)
    console.log(validate.name, `params: ${JSON.stringify(body)}`)

    if (!Array.isArray(body) || body.length != length) {
        logInfo(validate.name, `validation failed, invalid format, length is not match`)
        console.log(validate.name, `validation failed, invalid format, length is not match`)

        ctx.status = 400
        return ctx.body = []
    }

    const maxId = (count * count) - 1
    logInfo(validate.name, `$maxId: ${maxId}`)
    console.log(validate.name, `$maxId: ${maxId}`)

    const blockUpperbound = body.find((block) => {
        const id = Number.parseInt(block) || 0
        return id > maxId
    })
    logInfo(validate.name, `$blocks: ${JSON.stringify(blockUpperbound)}`)
    console.log(validate.name, `$blocks: ${JSON.stringify(blockUpperbound)}`)

    if (blockUpperbound) {
        logInfo(validate.name, `upperbound id blocks, $blocks: ${JSON.stringify(blockUpperbound)}`)
        console.log(validate.name, `upperbound id blocks, $blocks: ${JSON.stringify(blockUpperbound)}`)

        ctx.status = 400
        return ctx.body = []
    }

    logInfo(validate.name, `validation passed`)
    console.log(validate.name, `validation passed`)
    return next()
}

async function validateBlocksChange(ctx, next) { }

async function validateBlocksStatus(ctx, next) { }