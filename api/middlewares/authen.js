const Log = require('../services/log')
const Block = require('../models/block')()
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

module.exports = {
    authenticate
}

async function authenticate(ctx, next) {
    const token = ctx.get('token')
    logInfo(authenticate.name, `$token: ${token}`)

    if (!token) {
        logInfo(authenticate.name, `authentication failed, invalid token`)
        logInfo(authenticate.name, `$token: ${token}`)

        console.log(authenticate.name, `authentication failed, invalid token`)
        console.log(authenticate.name, `$token: ${token}`)
        
        ctx.status = 401
        return ctx.body = {}
    }

    try {
        const players = await Block
            .find({ player: token })
            .select({
                id: 1,
                status: 1,
                player: 1,
            })
            .skip(0)
            .limit(1)
            .exec()

        logInfo(authenticate.name, `$players: ${JSON.stringify(players)}`)
        console.log(authenticate.name, `$players: ${JSON.stringify(players)}`)

        if (!players || players.length <= 0) {
            logInfo(authenticate.name, `authentication failed, not found players`)
            logInfo(authenticate.name, `$token: ${token}`)

            console.log(authenticate.name, `authentication failed, not found players`)
            console.log(authenticate.name, `$token: ${token}`)

            ctx.status = 401
            return ctx.body = {}
        }

        logInfo(authenticate.name, `authentication success`)
        console.log(authenticate.name, `authentication success`)

        ctx.token = token
        return next(players)
    } catch (error) {
        logError(authenticate.name, `$error: ${JSON.stringify(error)}`)
        ctx.status = 403
        ctx.body = {}
    }
}