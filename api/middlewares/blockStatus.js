const pad = require('pad')
const Log = require('../services/log')
const Block = require('../models/block')()
const { status, count } = require('../../config/block')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

module.exports = {
    validate
}

async function validate(ctx, next) {
    const ids = ctx.request.body
    const token = ctx.token

    logInfo(validate.name, `$params: ${JSON.stringify({ ids, token })}`)
    console.log(validate.name, `$params: ${JSON.stringify({ ids, token })}`)

    if (!Array.isArray(ids)) {
        return null
    }

    const blocks = await Block
        .find({
            id: { $in: ids },
            player: token,
        })
        .select({
            id: 1,
            status: 1,
            row: 1,
            column: 1,
        })
        .exec()

    console.log(validate.name, `$blocks: ${JSON.stringify(blocks)}`)

    // top:                      01      02
    // left/right:      10      {11}    {12}    13
    // bottom:                   21      22
    const neighborIds = []

    for (const block of blocks) {
        const { row, column } = block
        console.log(validate.name, `$row: ${row}`)
        console.log(validate.name, `$column: ${column}`)

        let left = column - 1
        if (left >= 0) {
            left = pad(2, `${left}`, `${row}`)
            neighborIds.push(left)
            console.log(validate.name, `$left: ${left}`)
        }

        let right = column + 1
        if (right <= count) {
            right = pad(2, `${right}`, `${row}`)
            neighborIds.push(right)
            console.log(validate.name, `$right: ${right}`)
        }

        let top = row - 1
        if (top >= 0) {
            top = pad(`${top}`, 2, `${column}`)
            neighborIds.push(top)
            console.log(validate.name, `$top: ${top}`)
        }

        let bottom = row + 1
        if (bottom <= count) {
            bottom = pad(`${bottom}`, 2, `${column}`)
            neighborIds.push(bottom)
            console.log(validate.name, `$bottom: ${bottom}`)
        }
    }
    console.log(validate.name, `$neighborIds: ${JSON.stringify(neighborIds)}`)

    const blockIds = blocks
        .map(block => block.id)
        .concat(neighborIds)
    console.log(validate.name, `$blockIds: ${JSON.stringify(blockIds)}`)

    const blockIdSet = new Set(blockIds)
    const blockUniqueIds = [...blockIdSet]
    console.log(validate.name, `$blockUniqueIds: ${JSON.stringify(blockUniqueIds)}`)

    const allBlocks = await Block
        .find({
            id: { $in: blockUniqueIds },
            player: token,
        })
        .select({
            id: 1,
            status: 1,
            row: 1,
            column: 1,
        })
        .exec()
    console.log(validate.name, `$neighborBlocks: ${JSON.stringify(allBlocks)}`)

    const nonemptyBlocks = allBlocks.filter(block => {
        return block.status != 'e'
    })
    console.log(validate.name, `$nonemptyBlocks: ${JSON.stringify(nonemptyBlocks)}`)

    if (nonemptyBlocks.length > 0) { 
        ctx.status = 403
        return ctx.body = []
    }

    ctx.blockIds = blockUniqueIds
    logInfo(validate.name, `validation passed`)
    console.log(validate.name, `validation passed`)
    return next()
}