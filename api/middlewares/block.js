const uuid = require('uuid/v4')
const Log = require('../services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

module.exports = {
    validatePlayerToken
}

async function validatePlayerToken(ctx, next) {
    const token = ctx.get('token')
    logInfo(validatePlayerToken.name, `token: ${token}`)

    if (token) {
        ctx.status = 201
        return ctx.body = []
    }

    ctx.token = uuid()
    logInfo(validatePlayerToken.name, `uuid: ${ctx.token}`)
    return next()
}