const _package = require('../../package')

const controller = require('../controllers/index')
const blockController = require('../controllers/block')
const battleshipController = require('../controllers/battleship')

const blockValidator = require('../middlewares/block')
const blockBoundaryValidator = require('../middlewares/blockBoundary')
const blockStatusValidator = require('../middlewares/blockStatus')
const authenticator = require('../middlewares/authen')

const { routes } = require('../../config/api')
const { status } = require('../../config/block')
const {
    index,
    start,
    placeBattleship
} = routes

module.exports = (koaRouter) => {

    koaRouter.get('/', controller.get)
    koaRouter.get(index, controller.get)
    koaRouter.get(start, blockValidator.validatePlayerToken, blockController.init)
    koaRouter.post('/api/v1/reset', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })


    koaRouter.post(

        placeBattleship,

        async (ctx, next) => {
            ctx.blocksLength = 4
            ctx.shipStatus = status.battleship
            ctx.reservedStatus = 'rb'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/battleship/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })


    koaRouter.post(`/api/v1/cruiser/1/`,
        async (ctx, next) => {
            ctx.blocksLength = 3
            ctx.shipStatus = status.cruiser1
            ctx.reservedStatus = 'rc1'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/cruiser/1/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/cruiser/2/`,
        async (ctx, next) => {
            ctx.blocksLength = 3
            ctx.shipStatus = status.cruiser1
            ctx.reservedStatus = 'rc2'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/cruiser/2/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })



    koaRouter.post(`/api/v1/destroyer/1/`,
        async (ctx, next) => {
            ctx.blocksLength = 2
            ctx.shipStatus = status.destroyer1
            ctx.reservedStatus = 'rd1'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/destroyer/1/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/destroyer/2/`,
        async (ctx, next) => {
            ctx.blocksLength = 2
            ctx.shipStatus = status.destroyer2
            ctx.reservedStatus = 'rd2'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/destroyer/2/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/destroyer/1/`,
        async (ctx, next) => {
            ctx.blocksLength = 2
            ctx.shipStatus = status.destroyer3
            ctx.reservedStatus = 'rd3'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/destroyer/3/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })


    koaRouter.post(`/api/v1/submarine/1/`,
        async (ctx, next) => {
            ctx.blocksLength = 1
            ctx.shipStatus = status.submarine1
            ctx.reservedStatus = 'rs1'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/submarine/1/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/submarine/2/`,
        async (ctx, next) => {
            ctx.blocksLength = 1
            ctx.shipStatus = status.submarine2
            ctx.reservedStatus = 'rs2'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/submarine/2/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/submarine/3/`,
        async (ctx, next) => {
            ctx.blocksLength = 1
            ctx.shipStatus = status.submarine3
            ctx.reservedStatus = 'rs3'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/submarine/3/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })
    koaRouter.post(`/api/v1/submarine/3/`,
        async (ctx, next) => {
            ctx.blocksLength = 1
            ctx.shipStatus = status.submarine4
            ctx.reservedStatus = 'rs4'
        },
        authenticator.authenticate,
        blockBoundaryValidator.validate,
        blockStatusValidator.validate,

        battleshipController.place
    )
    koaRouter.post('/api/v1/submarine/3/rotate', async (ctx, next) => {
        ctx.status = 201
        return ctx.body = []
    })


    return koaRouter
}