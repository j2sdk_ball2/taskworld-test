const path = require('path')

class Log {

    constructor(cwd, filename) {
        const currentPath = path.relative(cwd, filename)
        this.logInfo = require('debug')(`logInfo:${currentPath}`)
        this.logError = require('debug')(`logError:${currentPath}`)
    }

    get info() {
        return this.logInfo
    }
    get error() {
        return this.logError
    }
}
module.exports = Log