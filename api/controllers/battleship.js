const Log = require('../services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error
const Block = require('../models/block')()
const { count, status } = require('../../config/block')

module.exports = {
    place,
}

async function place(ctx, next) {
    const shipIds = ctx.request.body
    const reservedIds = ctx.blockIds
    const shipStatus = ctx.shipStatus
    const reservedStatus = ctx.reservedStatus
    const token = ctx.token

    logInfo(place.name, `$params: ${JSON.stringify({ shipIds, reservedIds, token })}`)
    console.log(place.name, `$params: ${JSON.stringify({ shipIds, reservedIds, token })}`)

    if (!reservedIds || !token) {
        return null
    }

    try {

        const _status = [status.battleship, reservedStatus]
        logInfo(place.name, `$_status: ${JSON.stringify(_status)}`)
        console.log(place.name, `$_status: ${JSON.stringify(_status)}`)

        const existingBlocks = await Block
            .find({
                status: { $in: _status },
                player: token,
            })
            .select({
                id: 1,
                status: 1,
                row: 1,
                column: 1,
            })
            .exec()

        logInfo(place.name, `$blocks: ${JSON.stringify(existingBlocks)}`)
        console.log(place.name, `$blocks: ${JSON.stringify(existingBlocks)}`)

        if (existingBlocks.length > 0) {
            for (const block of existingBlocks) {
                block.status = status.empty
                await block.save()
            }
        }

        const allBlocks = await Block
            .find({
                id: { $in: reservedIds },
                player: token,
            })
            .select({
                id: 1,
                status: 1,
                row: 1,
                column: 1,
            })
            .exec()

        for (const block of allBlocks) {
            block.status = reservedStatus
            await block.save()
        }

        const shipBlocks = await Block
            .find({
                id: { $in: shipIds },
                player: token,
            })
            .select({
                id: 1,
                status: 1,
                row: 1,
                column: 1,
            })
            .exec()
        logInfo(place.name, `$shipBlocks: ${JSON.stringify(shipBlocks)}`)
        console.log(place.name, `$shipBlocks: ${JSON.stringify(shipBlocks)}`)

        for (const block of shipBlocks) {
            block.status = shipStatus
            await block.save()
        }

        ctx.status = 201
        ctx.body = {
            token,
            existingBlocks
        }
        return next()
    } catch (error) {
        logError(`error: ${JSON.stringify(error)}`)
        ctx.status = 403
        ctx.body = {}
    }
}