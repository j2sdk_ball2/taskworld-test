const _package = require('../../package')

module.exports = {
    get
}

async function get(ctx, next) {
    ctx.status = 200
    ctx.body = {
        name: _package.name,
        version: _package.version,
        description: _package.description,
        main: _package.main,
        keyword: _package.keyword,
        repository: _package.repository,
        keywords: _package.keywords,
        author: _package.author,
        license: _package.license,
        homepage: _package.homepage
    }
    return next()
}