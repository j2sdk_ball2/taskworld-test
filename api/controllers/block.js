const Log = require('../services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error
const Block = require('../models/block')()
const { count } = require('../../config/block')

module.exports = {
    init,
}

async function init(ctx, next) {
    const token = ctx.token
    logInfo(init.name, `token: ${token}`)

    if (!token) {
        logInfo(init.name, `invalid token, token: ${token}`)
        ctx.status = 401
        ctx.body = {}
        return next()
    }

    try {
        const blocks = []

        for (let row = 0; row < count; row++) {
            for (let col = 0; col < count; col++) {

                const block = new Block()
                const id = `${row}${col}`

                block.id = id
                block.player = token
                block.row = row
                block.column = col
                await block.save()

                blocks.push({
                    id
                })
            }
        }

        logInfo(init.name, `blocks saved`)

        ctx.status = 201
        ctx.body = {
            token,
            blocks
        }
        return next()
    } catch (error) {
        logError(`error: ${JSON.stringify(error)}`)
        ctx.status = 403
        ctx.body = {}
    }
}