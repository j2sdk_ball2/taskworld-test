const mongoose = require('./index')()
const Log = require('../services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

let schema

module.exports = Block

function Block() {
    logInfo(Block.name)
    if (!schema) {
        schema = createSchema()
    }
    return mongoose.model('Block', schema)
}

function createSchema() {
    logInfo(createSchema.name)

    const Schema = mongoose.Schema
    const schema = new Schema({
        id: {
            type: String,
            required: true,
            unique: true,
        },
        status: {
            // e = empty
            
            // b = battleship
            
            // c1 = cruiser1
            // c2 = cruiser1
            
            // d1 = destroyer1
            // d2 = destroyer2
            // d3 = destroyer3

            // s1 = submarine1
            // s2 = submarine2
            // s3 = submarine3
            // s4 = submarine4
            
            // r + (b, c1, c2, d1, d2, d3, s1, s2, s3, s4) = reserved
            type: String,
            required: true,
            default: 'e',
        },
        player: {
            type: String,
            required: true,
        },
        row: {
            type: Number,
            required: true,
        },
        column: {
            type: Number,
            required: true,
        },
        alignment: {
            // h = horizontal
            // v = vertical
            type: String,
        }
    },
        {
            timestamps: true
        })

    return schema
}