const request = require('supertest')
const { expect } = require('chai')
const { routes } = require('../../../config/api')
const path = routes.index
const expectedResult = {
  "name": "taskworld-test",
  "version": "1.0.0",
  "description": "",
  "main": "api/index.js",
  "keywords": [
    
  ],
  "author": "",
  "license": "ISC"
}
let server

describe('test/integration', () => {
    describe(`#${path}`, () => {

        before(async () => {
            server = await require('../index').start()
        })

        it('Should return 200 (OK) with path /', async () => {

            await request(server.instance)
                .get(path)
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(200)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    expect(body).eql(expectedResult)
                })
        })

        it('Should return 200 (OK) with path /api/v1/', async () => {

            await request(server.instance)
                .get('/api/v1/')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(200)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    expect(body).eql(expectedResult)
                })
        })
    })
})