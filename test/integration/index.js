require('dotenv').config({
    path: 'env/.local',
    silent: true
})

const server = require('../../api/server')

module.exports = {
    start
}

async function start() {
    if (!start.instance) {
        console.log('starting server ...')

        try {
            const instance = await server.start()
            console.log('server started')
            start.instance = instance.listener
            return start
        } catch (error) {
            console.log('starting server failed')
            console.log(`error: ${JSON.stringify(error)}`)
            return null
        }
    }
    return start
}