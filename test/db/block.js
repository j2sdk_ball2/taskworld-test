const request = require('supertest')
const { expect } = require('chai')
const { routes } = require('../../config/api')
const Block = require('../../api/models/block')()
const path = routes.index
let server

async function get() {
    await Block
        .findOne({ id: /^0123456789/i })
        .select({
            id: 1,
            status: 1,
            player: 1,
        })
        .exec()
}

describe('test/integration', () => {
    describe(`#${path}`, () => {

        before(async () => {
            await Block
                .find({ id: /^0123456789/i })
                .remove()
                .exec()
        })

        it('Should be able to save block', async () => {

            let block = new Block()
            block.id = '0123456789'
            block.player = 'f005f51d-95ac-4079-bed5-dc01d9f3af0c'
            await block.save()

            block = await get()

            const { id, status, player } = block
            expect(id).to.equal('0123456789')
            expect(status).to.equal(0)
            expect(player).to.equal('f005f51d-95ac-4079-bed5-dc01d9f3af0c')
        })

        it('Should be able to update block', async () => {

            let block = await get()

            block = new Block()
            block.id = '0123456789012'
            block.status = 'b'
            block.player = 'player001'
            await block.save()

            block = await get()

            const { id, status, player } = block
            expect(id).to.equal('0123456789012')
            expect(status).to.equal(1)
            expect(player).to.equal('player001')
        })
    });
})