const request = require('supertest')
const { expect } = require('chai')
const blockStatus = require('../../../api/middlewares/blockStatus')
const token = '5c1e26c2-733c-48d1-8f53-f6285b00a402'

describe('test/unit', () => {

    before(async () => {
    })

    describe(`#validateBlocksStatus`, () => {

        it('Should return next if all blocks status are empty', async () => {

            const ctx = {
                request: {
                    body: ['11', '12', '13', '14']
                },
                token
            }
            const next = () => {
                return true
            }

            const result = await blockStatus.validate(ctx, next)
            expect(result).to.be.true
        })
    })
})