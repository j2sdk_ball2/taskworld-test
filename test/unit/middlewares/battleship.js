const request = require('supertest')
const { expect } = require('chai')
const boundary = require('../../../api/middlewares/blockBoundary')
const blockConsecutive = require('../../../api/middlewares/blockConsecutive')

describe('test/unit', () => {

    before(async () => {
    })

    describe(`#validateBlocksBoundary`, () => {

        it('Should return [] (400) if params is not array', async () => {

            const ctx = {
                request: {
                    body: {}
                },
                blocksLength: 4
            }
            const next = () => {
                return true
            }

            const result = await boundary.validate(ctx, next)
            expect(ctx.status).to.equal(400)
            expect(result).to.eql([])
        })

        it('Should return [] (400) if params is not 4 elements array', async () => {

            const ctx = {
                request: {
                    body: ['00', '01', '02', '03', '04']
                },
                blocksLength: 4
            }
            const next = () => {
                return true
            }

            const result = await boundary.validate(ctx, next)
            expect(ctx.status).to.equal(400)
            expect(result).to.eql([])
        })

        it('Should return [] (400) if any elements in params are upperbound max id', async () => {

            const ctx = {
                request: {
                    body: ['00', '01', '02', '100']
                },
                blocksLength: 4
            }
            const next = () => {
                return true
            }

            const result = await boundary.validate(ctx, next)
            expect(ctx.status).to.equal(400)
            expect(result).to.eql([])
        })

        it('Should return next on validation success of horizontal blocks', async () => {

            const ctx = {
                request: {
                    body: ['00', '01', '02', '03']
                },
                blocksLength: 4
            }
            const next = () => {
                return true
            }

            const result = await boundary.validate(ctx, next)
            expect(result).to.equal(true)
        })
    })

    describe(`#validateBlockConsecutive`, () => {

        it('Should return null if params is not array', async () => {

            const ctx = {
                request: {
                    body: {}
                }
            }
            const next = () => {
                return true
            }

            const result = await blockConsecutive.validate(ctx, next)
            expect(result).to.be.null
        })

        it('Should return [] (400) if params elements is not consecutive horizontally', async () => {

            const ctx = {
                request: {
                    body: ['00', '02', '04', '06']
                }
            }
            const next = () => {
                return true
            }

            const result = await blockConsecutive.validate(ctx, next)
            expect(ctx.status).to.equal(400)
            expect(result).to.eql([])
        })

        it('Should return next on validation success of horizontal blocks', async () => {

            const ctx = {
                request: {
                    body: ['00', '01', '02', '03']
                }
            }
            const next = () => {
                return true
            }

            const result = await blockConsecutive.validate(ctx, next)
            expect(result).to.equal(true)
        })

        it('Should return next on validation success of vertical blocks', async () => {

            const ctx = {
                request: {
                    body: ['00', '10', '20', '30']
                }
            }
            const next = () => {
                return true
            }

            const result = await blockConsecutive.validate(ctx, next)
            expect(result).to.equal(true)
        })
    })

    describe(`#validateBlocksStatus`, () => {
    })
})