const { expect } = require('chai')
const Block = require('../../../api/models/block')()
const authen = require('../../../api/middlewares/authen')

const _id = '0123456789'
const token = 'f005f51d-95ac-4079-bed5-dc01d9f3af0c'

describe('test/unit', () => {

    before(async () => {
        await Block
            .findOne({ id: _id })
            .remove()
            .exec()

        let block = new Block()
        block.id = _id
        block.player = token
        block.row = 0
        block.column = 0
        await block.save()

        block = await Block
            .findOne({ id: _id })
            .select({
                id: 1,
                status: 1,
                player: 1,
            })
            .exec()

        const { id, status, player } = block

        expect(id).to.equal(_id)
        expect(status).to.equal('e')
        expect(player).to.equal(token)
    })

    describe(`#authenticate`, () => {

        it('Should return next on authentication success', async () => {

            const ctx = {
                get: (key) => {
                    return token
                }
            }
            const next = (players) => {
                return players
            }

            const result = await authen.authenticate(ctx, next)
            expect(Array.isArray(result)).to.be.true
            expect(result.length).to.equal(1)

            const { player } = result[0]
            expect(player).to.equal(token)
        })

        it('Should return {} (401) with non-tokenized request', async () => {

            const ctx = {
                get: (key) => {
                    return null
                }
            }
            const next = (players) => {
                return players
            }

            const result = await authen.authenticate(ctx, next)
            expect(ctx.status).to.equal(401)
            expect(result).to.eql({})
        })

        it('Should return {} (401) with fake token request', async () => {

            const ctx = {
                get: (key) => {
                    return 'f005f51d-xxxx-xxxx-xxxx-dc01d9f3af0c'
                }
            }
            const next = (players) => {
                return players
            }

            const result = await authen.authenticate(ctx, next)
            expect(ctx.status).to.equal(401)
            expect(result).to.eql({})
        })
    })
})