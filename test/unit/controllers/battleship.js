const request = require('supertest')
const { expect } = require('chai')
const controller = require('../../../api/controllers/battleship')
const { status } = require('../../../config/block')
const token = '5c1e26c2-733c-48d1-8f53-f6285b00a402'

describe('test/unit/controllers/battleship', () => {

    before(async () => {
    })

    describe(`#place`, () => {

        it('Should return next if all blocks status are empty', async () => {

            const ctx = {
                request: {
                    body: ['11', '12', '13', '14']
                },
                blockIds: ['11', '12', '13', '14', '10', '01', '21', '02', '22', '03', '23', '15', '04', '24'],
                shipStatus: status.battleship,
                reservedStatus: 'rb',
                token,
            }
            const next = () => {
                return true
            }

            const result = await controller.place(ctx, next)
            expect(ctx.status).to.equal(201)
            expect(result).to.be.true
        })
    })
})