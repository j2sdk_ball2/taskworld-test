import React, { Component } from 'react'
import Row from './row'

export default class Board extends Component {

    constructor(props) {
        super(props)

        const rows = [
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
        ]
        const cells = [
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
        ]

        this.state = {
            rows,
            cells
        }
    }

    render() {
        const { rows, cells } = this.state

        return (
            <div>
                <table>
                    <tbody>
                        {rows.map((row, index) => {
                            return (<Row key={index} rowNo={index} cells={cells} />)
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}