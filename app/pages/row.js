import React, { Component } from 'react'
import '../style.css'

const row = (props) => {
    const { cells } = props
    return (
        <tr>
            {cells.map((cell, index) => {
                return (<td key={index} className="table-cell">&nbsp;</td>)
            })}
        </tr>
    )
}

export default row