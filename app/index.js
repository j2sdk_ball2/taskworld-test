import React from 'react'
import ReactDOM from 'react-dom'
import Board from './pages/board'

ReactDOM.render(
  <Board />,
  document.getElementById('app')
)