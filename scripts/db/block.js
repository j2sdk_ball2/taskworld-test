module.exports = {
    clean
}

async function clean() {
    const Block = require('../../api/models/block')()
    await Block
        .remove()
        .exec()
}